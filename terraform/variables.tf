# Defining Region
variable "aws_region" {
  default = "eu-west-2"
}

# Defining CIDR Block for VPC
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
